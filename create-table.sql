
create table t_appointment
(id bigint generated always as identity primary key not null,
customer_id bigint null,
doctor_office_id bigint null,
doctor_office_schedule_id bigint null,
doctor_office_treatment_id bigint null,
appointment_date date null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);

create table t_appointment_cancellation
(id bigint generated always as identity primary key not null,
appointment_id bigint null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);

create table t_appointment_done
(id bigint generated always as identity primary key not null,
appointment_id bigint null,
diagnosis varchar(255) not null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);

create table t_appointment_reschedule_history
(id bigint generated always as identity primary key not null,
appointment_id bigint null,
doctor_office_schedule_id bigint null,
doctor_office_treatment_id bigint null,
appointment_date date null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);

create table t_current_doctor_specialization
(id bigint generated always as identity primary key not null,
doctor_id bigint null,
specialization_id bigint null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);

CREATE TABLE m_customer_relation (
id bigint generated always as identity primary key not null,
name varchar(50) null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);

CREATE TABLE m_doctor (
id bigint generated always as identity primary key not null,
biodata_id bigint null,
str varchar(50) null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);

CREATE TABLE m_doctor_education (
 id bigint generated always as identity primary key not null,
 doctor_id BIGINT NULL,
 education_level_id BIGINT NULL,
 institution_name VARCHAR(100) null,
major varchar(100) null,
start_year varchar(4) null,
end_year varchar(4) null,
 is_last_education BOOLEAN null default false,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);

CREATE TABLE m_education_level (
id bigint generated always as identity primary key not null,
name varchar(10) null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);

CREATE TABLE m_location (
id bigint generated always as identity primary key not null,
name varchar(100) null,
parent_id bigint null,
location_level_id bigint null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);


create table m_admin (
id bigint generated always as identity primary key not null,
biodata_id bigint null,
code varchar(10) null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null
);

create table m_bank (
id bigint generated always as identity primary key not null,
name varchar(255),
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null
);

create table m_biodata (
id bigint generated always as identity primary key not null,
fullname varchar(255) null,
mobile_phone varchar(15) null,
image path null,
image_path varchar(255) null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null
);

create table m_biodata_adress(
id bigint generated always as identity primary key not null,
biodata_id bigint null,
label varchar(100) null,
recipient varchar(100) null,
recipient_phone_number varchar(15) null,
location_id bigint null,
postal_code varchar(10) null,
adress text null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null
);

create table m_blood_group(
    id bigint generated always as identity primary key not null,
    code varchar(5) null,
    description varchar(255) null,
    created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null
);



CREATE TABLE m_courier (
    id bigint generated always as identity primary key not null,
    name varchar(50) null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);

CREATE TABLE m_courier_type (
    id bigint generated always as identity primary key not null,
    courier_id bigint null,
    name varchar(20) null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);
CREATE TABLE t_courier_discount (
    id bigint generated always as identity primary key not null,
    courier_type_id bigint null,
    value decimal null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);

CREATE TABLE m_customer (
    id bigint generated always as identity primary key not null,
    biodata_id bigint null,
    bod date null,
    gender varchar(1) null,
    blood_group_id bigint null,
    rhesus_type varchar(5) null,
    height decimal null,
    weight decimal null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);

CREATE TABLE m_customer_member (
    id bigint generated always as identity primary key not null,
    customer_id bigint null,
    customer_relation_id bigint null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);



CREATE TABLE m_location_level (
    id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50),
    abbreviation varchar(50),
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_facility (
     id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
    medical_facility_category_id bigint ,
    location_id bigint,
    full_address text,
    email varchar(100) NOT NULL,
    phone_code varchar(10) NOT NULL,
    phone varchar(15) NOT NULL,
    fax varchar(15) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_facility_category (
     id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_facility_schedule (
     id bigint generated always as identity PRIMARY KEY not null,
    medical_facility_id bigint,
    day varchar(10) NOT NULL,
    time_schedule_start varchar(10) NOT NULL,
    time_schedule_end varchar(10) NOT NULL,
     created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_item (
     id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
    medical_item_category_id bigint,
    composition text,
    medical_item_segmentation_id bigint ,
    manufacturer varchar(100) NOT NULL,
    indication text,
    dosage text,
    directions text NOT NULL,
    contraindication text,
    caution text,
    packaging varchar(50) NOT NULL,
    price_max bigint,
    price_min bigint,
    image path, 
    image_path varchar(100) NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_item_category (
    id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_medical_item_segmentation (
   id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);

CREATE TABLE m_menu (
     id bigint generated always as identity PRIMARY KEY not null,
    name varchar(20) NOT NULL,
    url varchar(50) NOT NULL,
    parent_id bigint,
    big_icon varchar(100) NOT NULL,
    small_icon varchar(100) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_menu_role (
     id bigint generated always as identity PRIMARY KEY not null,
    menu_id bigint ,
    role_id bigint , 
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_payment_method (
    id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_role (
     id bigint generated always as identity PRIMARY KEY not null,
    name varchar(20) NOT NULL,
    code varchar(20) NOT NULL,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_specialization (
    id bigint generated always as identity PRIMARY KEY not null,
    name varchar(50) NOT NULL,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_user (
    id bigint generated always as identity PRIMARY KEY not null,
    biodata_id bigint ,
    role_id bigint ,
    email varchar(100) NOT NULL,
    password varchar(255) NOT NULL,
    login_attempt int,
    is_locked boolean,
    last_login timestamp,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_biodata_attachment (
   id bigint generated always as identity PRIMARY KEY not null,
    biodata_id bigint , 
    file_name varchar(50) NOT NULL,
    file_path varchar(100) NOT NULL,
    file_size int NOT NULL,
    file bytea,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE m_wallet_default_nominal (
     id bigint generated always as identity PRIMARY KEY not null,
    nominal int,
   created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_doctor_office_treatment (
    id bigint generated always as identity PRIMARY KEY not null,
    doctor_treatment_id bigint ,
    doctor_office_id bigint , 
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_doctor_office_treatment_price (
    id bigint generated always as identity PRIMARY KEY not null,
    doctor_office_treatment_id bigint,
    price decimal,
    price_start_from decimal,
    price_until_from decimal,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_doctor_treatment (
    id bigint generated always as identity PRIMARY KEY not null,
    doctor_id bigint ,
    name varchar(50) NOT NULL,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_medical_item_purchase (
    id bigint generated always as identity PRIMARY KEY not null,
    customer_id bigint,
    payment_method_id bigint,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_medical_item_purchase_detail (
    id bigint generated always as identity PRIMARY KEY not null,
    medical_item_purchase_id bigint ,
    medical_item_id bigint , 
    qty int,
    medical_facility_id bigint , 
    courier_id bigint , 
    sub_total decimal,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_reset_password (
    id bigint generated always as identity PRIMARY KEY not null,
    old_password varchar(255),
    new_password varchar(255),
    reset_for varchar(20),
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_token (
    id bigint generated always as identity PRIMARY KEY not null,
    email varchar(100) NOT NULL,
    user_id bigint , 
    token varchar(50) NOT NULL,
    expired_on timestamp,
    is_expired boolean,
    used_for varchar(20),
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_treatment_discount (
    id bigint generated always as identity PRIMARY KEY not null,
    doctor_office_treatment_price_id bigint ,
    value decimal,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_customer_wallet_withdraw (
    id bigint generated always as identity PRIMARY KEY not null,
    customer_id bigint , 
    wallet_default_nominal_id bigint , 
    amount int NOT NULL,
    bank_name varchar(50) NOT NULL,
    account_number varchar(50) NOT NULL,
    account_name varchar(255) NOT NULL,
    otp int NOT NULL,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);
CREATE TABLE t_prescription (
    id bigint generated always as identity PRIMARY KEY not null,
    appointment_id bigint ,
    medical_item_id bigint , 
    dosage text,
    directions text NOT NULL,
    time varchar(100),
    notes text,
	created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean not null
);


CREATE TABLE t_customer_va_history (
    id BIGINT generated always as identity PRIMARY KEY not null,
    customer_va_id BIGINT null,
    amount DECIMAL null,
    expired_on TIMESTAMP null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);

CREATE TABLE t_customer_wallet (
    id BIGINT generated always as identity PRIMARY KEY not null,
    customer_id BIGINT null,
    pin VARCHAR(6) null,
    balance DECIMAL null,
    barcode VARCHAR(50) null,
    points DECIMAL null,
    created_by bigint not null,
    created_on timestamp not null,
    modified_by bigint null,
    modified_on timestamp null,
    deleted_by bigint null,
    deleted_on timestamp null,
    is_delete boolean not null default false
);


CREATE TABLE t_customer_wallet_top_up(
id BIGINT generated always as identity PRIMARY KEY not null,
customer_wallet_id bigint null,
amount decimal null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false
);
CREATE TABLE t_doctor_office(
id BIGINT generated always as identity PRIMARY KEY not null,
doctor_id bigint null,
medical_facility_id bigint null,
specialization varchar(100) not null,
start_date date not null,
end_date date not null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);


CREATE TABLE t_doctor_office_schedule(
id BIGINT generated always as identity PRIMARY KEY not null,
doctor_id bigint null,
medical_facility_schedule_id bigint null,
slot int null,
created_by bigint not null,
created_on timestamp not null,
modified_by bigint null,
modified_on timestamp null,
deleted_by bigint null,
deleted_on timestamp null,
is_delete boolean not null default false);



CREATE TABLE t_customer_chat (
    id bigint generated always as identity primary key not null,
    customer_id bigint,
    doctor_id bigint,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean NOT NULL DEFAULT FALSE);
CREATE TABLE t_customer_chat_history (
    id bigint generated always as identity primary key not null,
    customer_chat_id bigint,
    chat_content text,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean NOT NULL DEFAULT FALSE);
CREATE TABLE t_customer_custom_nominal (
    id bigint generated always as identity primary key not null,
    customer_id bigint,
    nominal int,
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean NOT NULL DEFAULT FALSE);
CREATE TABLE t_customer_registered_card (
    id bigint generated always as identity primary key not null,
    customer_id bigint,
    card_number varchar(20),
    validity_period date,
    cvv varchar(5),
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean NOT NULL DEFAULT FALSE);
CREATE TABLE t_customer_va (
    id bigint generated always as identity primary key not null,
    customer_id bigint,
    va_number varchar(30),
    created_by bigint NOT NULL,
    created_on timestamp NOT NULL,
    modified_by bigint,
    modified_on timestamp,
    deleted_by bigint,
    deleted_on timestamp,
    is_delete boolean NOT NULL DEFAULT FALSE);
