package com.minpro.team2;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.minpro.team2.controller.ApiProfileUserController;

@SpringBootApplication
public class Java327Team2Application {

	public static void main(String[] args) {
		SpringApplication.run(Java327Team2Application.class, args);
	}

}
