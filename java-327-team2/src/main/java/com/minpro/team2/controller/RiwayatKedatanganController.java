package com.minpro.team2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("riwayatkedatangan")
public class RiwayatKedatanganController {

	@GetMapping("indexapi/{id}")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/riwayatkedatangan/indexapi");
		
		return view;
	}
	
}
