package com.minpro.team2.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.minpro.team2.model.Appointment;
import com.minpro.team2.model.AppointmentDone;
import com.minpro.team2.model.Prescription;
import com.minpro.team2.repository.AppointmentDoneRepository;
import com.minpro.team2.repository.AppointmentRepository;
import com.minpro.team2.repository.PrescriptionRepository;

@RestController
@RequestMapping("/api/")
public class ApiRiwayatKedatanganController {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private AppointmentDoneRepository appointmentDoneRepository;

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	// PAGINATION
	@GetMapping("getAppointmentByParentBioId/paging")
	public ResponseEntity<Map<String, Object>> getAllPasienMember(@RequestParam(defaultValue = "0") int currentPage,
			@RequestParam(defaultValue = "5") int size, @RequestParam("keyword") String keyword,
			@RequestParam("sortType") String sortType, @RequestParam("urutan") String urutan,
			@RequestParam("id") Long id) {

		try {
			List<Appointment> role = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage, size);

			Page<Appointment> pages = null;
			if (urutan.equals("nama")) {
				if (sortType.equals("ASC")) {
					pages = this.appointmentRepository.findByIsDelete(keyword, false, id, pagingSort);
				} else {
					pages = this.appointmentRepository.findByIsDeleteDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("tanggal kedatangan")) {
				if (sortType.equals("ASC")) {
					pages = this.appointmentRepository.findByIsDeleteAppointmentDate(keyword, false, id, pagingSort);
				} else {
					pages = this.appointmentRepository.findByIsDeleteAppointmentDateDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("tanggal dibuat")) {
				if (sortType.equals("ASC")) {
					pages = this.appointmentRepository.findByIsDeleteCreatedOn(keyword, false, id, pagingSort);
				} else {
					pages = this.appointmentRepository.findByIsDeleteCreatedOnDESC(keyword, false, id, pagingSort);
				}
			}
			/*
			 * else if(urutan.equals("usia")) { if (sortType.equals("ASC")) { pages =
			 * this.appointmentRepository.findByIsDeleteUsia(keyword, false, id,
			 * pagingSort); } else { pages =
			 * this.appointmentRepository.findByIsDeleteUsiaDESC(keyword, false, id,
			 * pagingSort); } } else if(urutan.equals("relasi")) { if
			 * (sortType.equals("ASC")) { pages =
			 * this.appointmentRepository.findByIsDeleteRelasi(keyword, false, id,
			 * pagingSort); } else { pages =
			 * this.appointmentRepository.findByIsDeleteRelasiDESC(keyword, false, id,
			 * pagingSort); } } else if(urutan.equals("chat")) { if (sortType.equals("ASC"))
			 * { pages = this.appointmentRepository.findByIsDeleteChat(keyword, false, id,
			 * pagingSort); } else { pages =
			 * this.appointmentRepository.findByIsDeleteChatDESC(keyword, false, id,
			 * pagingSort); } } else if(urutan.equals("appointment")) { if
			 * (sortType.equals("ASC")) { pages =
			 * this.appointmentRepository.findByIsDeleteAppointment(keyword, false, id,
			 * pagingSort); } else { pages =
			 * this.appointmentRepository.findByIsDeleteAppointmentDESC(keyword, false, id,
			 * pagingSort); } }
			 */

			role = pages.getContent();

			Map<String, Object> response = new HashMap<>();

			response.put("pages", pages.getNumber());
			response.put("total", pages.getTotalElements());
			response.put("total_pages", pages.getTotalPages());
			response.put("data", role);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("getDetailsAppointment")
	public ResponseEntity<Object> getDetailsAppointment(@RequestParam("id") Long id) {
		try {
			AppointmentDone ad = this.appointmentDoneRepository.getDetailsByAppointmentId(id);

			return new ResponseEntity<>(ad, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getPrescriptionAppointment")
	public ResponseEntity<List<Prescription>> getPrescriptionAppointment(@RequestParam("id") Long id) {
		try {
			List<Prescription> prescription = this.prescriptionRepository.getPrescriptionByAppointmentId(id);

			return new ResponseEntity<>(prescription, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getAppointment")
	public ResponseEntity<Appointment> getAppointment(@RequestParam("id") Long id) {
		try {
			Appointment app = this.appointmentRepository.findByAppointmentId(id);
			return new ResponseEntity<>(app, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
