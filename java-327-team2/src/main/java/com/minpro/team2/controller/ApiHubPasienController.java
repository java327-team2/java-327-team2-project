package com.minpro.team2.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.management.relation.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.minpro.team2.model.CustomerRelation;
import com.minpro.team2.repository.CustomerRelationRepository;

@RestController
@RequestMapping("/api/")
public class ApiHubPasienController {

	@Autowired
	private CustomerRelationRepository customerRelationRepository;

	@GetMapping("customerrelation")
	public ResponseEntity<List<CustomerRelation>> getAllCustomerRelation(@RequestParam("keyword") String keyword) {
		try {
			List<CustomerRelation> customerRelation = this.customerRelationRepository.findByIsDelete(keyword, false);
			return new ResponseEntity<>(customerRelation, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("customerrelation/add")
	public ResponseEntity<Object> saveCustomerRelation(@RequestBody CustomerRelation cr) {
		cr.createdBy = Long.valueOf(1);
		cr.createdOn = new Date();
		cr.isDelete = false;

		CustomerRelation crData = this.customerRelationRepository.save(cr);

		if (crData.equals(cr)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("customerrelation/{id}")
	public ResponseEntity<Object> getCustomerRelationById(@PathVariable("id") Long id) {
		try {
			Optional<CustomerRelation> cr = this.customerRelationRepository.findById(id);
			return new ResponseEntity<>(cr, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("customerrelation/edit/{id}")
	public ResponseEntity<Object> editCustomerRelation(@PathVariable("id") Long id, @RequestBody CustomerRelation cr) {
		Optional<CustomerRelation> crData = this.customerRelationRepository.findById(id);

		if (crData.isPresent()) {
			cr.id = id;
			cr.modifiedBy = Long.valueOf(1);
			cr.modifiedOn = new Date();
			cr.createdBy = crData.get().createdBy;
			cr.createdOn = crData.get().createdOn;
			cr.isDelete = false;

			this.customerRelationRepository.save(cr);
			return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("customerrelation/delete/{id}")
	public ResponseEntity<Object> deleteCustomerRelation(@PathVariable("id") Long id) {
		Optional<CustomerRelation> crData = this.customerRelationRepository.findById(id);

		if (crData.isPresent()) {
			CustomerRelation cr = new CustomerRelation();
			cr.id = id;
//			cr.isActive = false;
			cr.setIsDelete(true);
			cr.deletedBy = Long.valueOf(1);
			cr.deletedOn = new Date();
			cr.modifiedBy = crData.get().modifiedBy;
			cr.modifiedOn = crData.get().modifiedOn;
			cr.createdBy = crData.get().createdBy;
			cr.createdOn = crData.get().createdOn;
			cr.name = crData.get().name;

			this.customerRelationRepository.save(cr);
			return new ResponseEntity<>("Deleted Data Success", HttpStatus.OK);

		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("customerrelation/checkNameIsExist")
	public ResponseEntity<Object> checkNameIsExist(@RequestParam("name") String name,
			@RequestParam("id") Long id) {

		Boolean isExist = false;
		CustomerRelation cr = null;
		if (id == 0) {
			cr = this.customerRelationRepository.findByIdName(name);
		} else {
			
			cr = this.customerRelationRepository.findByIdNameForEdit(name, id);
		}

		if (cr != null) {
			isExist = true;
		}

		return new ResponseEntity<>(isExist, HttpStatus.OK);
	}

}
