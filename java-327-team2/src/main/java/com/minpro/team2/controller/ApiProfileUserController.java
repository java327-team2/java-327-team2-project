package com.minpro.team2.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.minpro.team2.model.Biodata;
import com.minpro.team2.model.Customer;
import com.minpro.team2.model.User;
import com.minpro.team2.repository.BiodataRepository;
import com.minpro.team2.repository.CustomerRepository;
import com.minpro.team2.repository.UserRepository;

@RestController
@RequestMapping("/api/")
public class ApiProfileUserController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private BiodataRepository biodataRepository;
	
	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/img";

	
	@GetMapping("getUserById")
	public ResponseEntity<User> getUserById(@RequestParam("id") Long id){
		
		try {
			User user = this.userRepository.findById(id).orElse(null);
			
			return new ResponseEntity<>(user,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("getCustomerByBiodataId")
	public ResponseEntity<Customer> getCustomerByBiodataId(@RequestParam("biodata") Long id){
		
		try {
			Customer customer = this.customerRepository.findByBiodataId(id);
			
			return new ResponseEntity<>(customer, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PutMapping("updateImage/{userId}")
	public ResponseEntity<Object> updateImage(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id, @PathVariable("userId") Long userId){
		
		
		
		LocalDateTime datetime = LocalDateTime.now();
		DateTimeFormatter formatdate = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		
		String filename = datetime.format(formatdate) + "_" + file.getOriginalFilename();
		
		String imgdelete = "";

		Path fileNameAndPath = Paths.get(uploadDirectory, filename);
		
		try {
			
			Files.write(fileNameAndPath, file.getBytes());
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		Biodata bio = this.biodataRepository.findById(id).orElse(null);
		if(bio != null) {
			imgdelete = bio.imagePath;
			bio.imagePath = filename;
			bio.modifiedBy = userId;
			bio.modifiedOn = new Date();
		}
		Path fileNameAndPathDel = Paths.get(uploadDirectory, imgdelete);
		try {
			Files.delete(fileNameAndPathDel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.biodataRepository.save(bio);
		return new ResponseEntity<>(bio, HttpStatus.OK);
		
	}
	
}
