package com.minpro.team2.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.minpro.team2.model.Appointment;
import com.minpro.team2.model.Biodata;
import com.minpro.team2.model.BloodGroup;
import com.minpro.team2.model.Customer;
import com.minpro.team2.model.CustomerChat;
import com.minpro.team2.model.CustomerMember;
import com.minpro.team2.model.CustomerRelation;
import com.minpro.team2.model.VMCustomerMember;
import com.minpro.team2.repository.AppointmentRepository;
import com.minpro.team2.repository.BiodataRepository;
import com.minpro.team2.repository.BloodGroupRepository;
import com.minpro.team2.repository.CustomerChatRepository;
import com.minpro.team2.repository.CustomerMemberRepository;
import com.minpro.team2.repository.CustomerRelationRepository;
import com.minpro.team2.repository.CustomerRepository;

@RestController
@RequestMapping("/api/")
public class ApiPasienController {

	@Autowired
	private CustomerMemberRepository customerMemberRepository;

	@Autowired
	private CustomerChatRepository customerChatRepository;

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private BloodGroupRepository bloodGroupRepository;

	@Autowired
	private CustomerRelationRepository customerRelationRepository;

	@Autowired
	private BiodataRepository biodataRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping("getMemberByParentBioId/{id}")
	public ResponseEntity<List<CustomerMember>> getMemberByParentBioId(@PathVariable("id") Long id) {
		try {
			List<CustomerMember> cm = this.customerMemberRepository.findByParentBioId(id);
			return new ResponseEntity<>(cm, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getChat/{id}")
	public ResponseEntity<List<CustomerChat>> getChat(@PathVariable("id") Long id) {
		try {
			List<CustomerChat> cc = this.customerChatRepository.findChatByCustomerId(id);
			return new ResponseEntity<>(cc, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getAppointment/{id}")
	public ResponseEntity<List<Appointment>> getAppointment(@PathVariable("id") Long id) {
		try {
			List<Appointment> appointment = this.appointmentRepository.findAppointmentByCustomerId(id);
			return new ResponseEntity<>(appointment, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getBloodGroup")
	public ResponseEntity<List<BloodGroup>> getBloodGroup() {
		try {
			List<BloodGroup> bg = this.bloodGroupRepository.findAll();
			return new ResponseEntity<>(bg, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("getCustomerRelation")
	public ResponseEntity<List<CustomerRelation>> getCustomerRelation() {
		try {
			List<CustomerRelation> cr = this.customerRelationRepository.findAll();
			return new ResponseEntity<>(cr, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("saveCustomerMember")
	public ResponseEntity<Object> saveCustomerMember(@RequestBody VMCustomerMember vmcr) {
		try {
			Biodata bio = new Biodata();
			bio.fullName = vmcr.fullName;
			bio.createdBy = vmcr.userId;
			bio.createdOn = new Date();
			bio.isDelete = false;
			this.biodataRepository.save(bio);

			Customer customer = new Customer();
			customer.biodataId = bio.id;
			customer.dob = vmcr.dob;
			customer.gender = vmcr.gender;
			customer.bloodGroupId = vmcr.bloodGroupId;
			customer.rhesusType = vmcr.rhesusType;
			customer.height = vmcr.height;
			customer.weight = vmcr.weight;
			customer.createdBy = vmcr.userId;
			customer.createdOn = new Date();
			customer.isDelete = false;

			this.customerRepository.save(customer);
			CustomerMember cm = new CustomerMember();
			cm.parentBioId = vmcr.id;
			cm.customerId = customer.id;
			cm.customerRelationId = vmcr.customerRelationId;
			cm.createdBy = vmcr.userId;
			cm.createdOn = new Date();
			cm.isDelete = false;

			this.customerMemberRepository.save(cm);

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("getMemberById/{id}")
	public ResponseEntity<Object> getMemberById(@PathVariable("id") Long id) {
		try {
			CustomerMember cm = this.customerMemberRepository.findById(id).orElse(null);
			return new ResponseEntity<>(cm, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("editCustomerMember/{id}")
	public ResponseEntity<Object> editCustomerMember(@PathVariable("id") Long id, @RequestBody VMCustomerMember vmcr) {
		try {
			Optional<CustomerMember> cmData = this.customerMemberRepository.findById(id);
			Optional<Customer> cData = this.customerRepository.findById(cmData.get().customerId);

			Optional<Biodata> bioData = this.biodataRepository.findById(cmData.get().customer.biodata.id);
			CustomerMember cm = this.customerMemberRepository.findById(id).orElse(null);
			Customer customer = this.customerRepository.findById(cm.customerId).orElse(null);
			Biodata bio = this.biodataRepository.findById(customer.biodataId).orElse(null);

			if (bioData.isPresent()) {
				bio.fullName = vmcr.fullName;
				bio.modifiedBy = vmcr.userId;
				bio.modifiedOn = new Date();

				this.biodataRepository.save(bio);
			}
			if (cData.isPresent()) {
				customer.dob = vmcr.dob;
				customer.gender = vmcr.gender;
				customer.bloodGroupId = vmcr.bloodGroupId;
				customer.rhesusType = vmcr.rhesusType;
				customer.height = vmcr.height;
				customer.weight = vmcr.weight;
				customer.modifiedBy = vmcr.userId;
				customer.modifiedOn = new Date();

				this.customerRepository.save(customer);
			}
			if (cmData.isPresent()) {
				cm.customerRelationId = vmcr.customerRelationId;
				cm.modifiedBy = vmcr.userId;
				cm.modifiedOn = new Date();

				this.customerMemberRepository.save(cm);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("deletePasien/{id}")
	public ResponseEntity<Object> deletePasien(@PathVariable("id") Long id) {
		try {
			CustomerMember cm = this.customerMemberRepository.findById(id).orElse(null);
			Customer c = this.customerRepository.findById(cm.customerId).orElse(null);
			Biodata b = this.biodataRepository.findById(c.biodataId).orElse(null);

			cm.deletedBy = cm.createdBy;
			cm.deletedOn = new Date();
			cm.isDelete = true;

			c.deletedBy = cm.deletedBy;
			c.deletedOn = cm.deletedOn;
			c.isDelete = true;

			b.deletedBy = c.deletedBy;
			b.deletedOn = c.deletedOn;
			b.isDelete = true;

			this.customerMemberRepository.save(cm);
			this.customerRepository.save(c);
			this.biodataRepository.save(b);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	// PAGINATION
	@GetMapping("getMemberByParentBioId/paging")
	public ResponseEntity<Map<String, Object>> getAllPasienMember(@RequestParam(defaultValue = "0") int currentPage,
			@RequestParam(defaultValue = "5") int size, @RequestParam("keyword") String keyword,
			@RequestParam("sortType") String sortType, @RequestParam("urutan") String urutan,
			@RequestParam("id") Long id) {

		try {
			List<CustomerMember> role = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(currentPage, size);

			Page<CustomerMember> pages = null;
			if (urutan.equals("nama")) {
				if (sortType.equals("ASC")) {
					pages = this.customerMemberRepository.findByIsDelete(keyword, false, id, pagingSort);
				} else {
					pages = this.customerMemberRepository.findByIsDeleteDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("usia")) {
				if (sortType.equals("ASC")) {
					pages = this.customerMemberRepository.findByIsDeleteUsia(keyword, false, id, pagingSort);
				} else {
					pages = this.customerMemberRepository.findByIsDeleteUsiaDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("relasi")) {
				if (sortType.equals("ASC")) {
					pages = this.customerMemberRepository.findByIsDeleteRelasi(keyword, false, id, pagingSort);
				} else {
					pages = this.customerMemberRepository.findByIsDeleteRelasiDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("chat")) {
				if (sortType.equals("ASC")) {
					pages = this.customerMemberRepository.findByIsDeleteChat(keyword, false, id, pagingSort);

				} else {
					pages = this.customerMemberRepository.findByIsDeleteChatDESC(keyword, false, id, pagingSort);
				}
			} else if (urutan.equals("appointment")) {
				if (sortType.equals("ASC")) {
					pages = this.customerMemberRepository.findByIsDeleteAppointment(keyword, false, id, pagingSort);
				} else {
					pages = this.customerMemberRepository.findByIsDeleteAppointmentDESC(keyword, false, id, pagingSort);
				}
			}

			role = pages.getContent();

			Map<String, Object> response = new HashMap<>();

			response.put("pages", pages.getNumber());
			response.put("total", pages.getTotalElements());
			response.put("total_pages", pages.getTotalPages());
			response.put("data", role);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("pasien/checkNameIsExist")
	public ResponseEntity<Object> checkNameIsExist(@RequestParam("name") String name, @RequestParam("id") Long id) {

		Boolean isExist = false;
		CustomerMember cr = null;
		if (id == 0) {
			cr = this.customerMemberRepository.findByIdName(name);
		} else {

			cr = this.customerMemberRepository.findByIdNameForEdit(name, id);
		}

		if (cr != null) {
			isExist = true;
		}

		return new ResponseEntity<>(isExist, HttpStatus.OK);
	}

	@GetMapping("pasien/checkNameIsExistNew")
	public ResponseEntity<Object> checkNameIsExist(@RequestParam("name") String name, @RequestParam("id") Long id,
			@RequestParam("userId") Long userId) {

		Boolean isExist = false;
		CustomerMember cr = null;
		if (id == 0) {
			cr = this.customerMemberRepository.findByIdName(name);
		} else {

			cr = this.customerMemberRepository.findByIdNameForEditNew(name, id, userId);
		}

		if (cr != null) {
			isExist = true;
		}

		return new ResponseEntity<>(isExist, HttpStatus.OK);
	}

}
