package com.minpro.team2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/customerrelation/")
public class HubPasienController {

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("hub-pasien/index");
		
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("hub-pasien/indexapi");
		return view;
	}
	
}
