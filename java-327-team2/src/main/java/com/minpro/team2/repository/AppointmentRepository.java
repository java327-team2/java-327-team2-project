package com.minpro.team2.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.Appointment;
import com.minpro.team2.model.CustomerMember;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	@Query(value = "select * from t_appointment where customer_id = ?1", nativeQuery = true)
	public List<Appointment> findAppointmentByCustomerId(Long id);

	// PAGING//
	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by b.fullName asc", nativeQuery = true)
	Page<Appointment> findByIsDelete(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by b.fullName desc", nativeQuery = true)
	Page<Appointment> findByIsDeleteDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by a.appointment_date asc", nativeQuery = true)
	Page<Appointment> findByIsDeleteAppointmentDate(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by a.appointment_date desc", nativeQuery = true)
	Page<Appointment> findByIsDeleteAppointmentDateDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by a.created_on asc", nativeQuery = true)
	Page<Appointment> findByIsDeleteCreatedOn(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select a.* from t_appointment a join m_customer c on a.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_member cm on cm.customer_id = c.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 and a.appointment_date < now() order by a.created_on desc", nativeQuery = true)
	Page<Appointment> findByIsDeleteCreatedOnDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select * from t_appointment where id = ?1", nativeQuery = true)
	public Appointment findByAppointmentId(Long id);

}
