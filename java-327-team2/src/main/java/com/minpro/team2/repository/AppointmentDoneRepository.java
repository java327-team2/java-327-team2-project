package com.minpro.team2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.AppointmentDone;

public interface AppointmentDoneRepository extends JpaRepository<AppointmentDone, Long> {

	@Query(value="select * from t_appointment_done where appointment_id = ?1",nativeQuery = true)
	public AppointmentDone getDetailsByAppointmentId(Long AppointmentId);
	
}
