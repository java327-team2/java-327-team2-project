package com.minpro.team2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.CustomerChat;

public interface CustomerChatRepository extends JpaRepository<CustomerChat, Long>{

	@Query(value = "select * from t_customer_chat where customer_id = ?1",nativeQuery = true)
	public List<CustomerChat> findChatByCustomerId(Long id);
	
	
}
