package com.minpro.team2.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.CustomerRelation;

public interface CustomerRelationRepository extends JpaRepository<CustomerRelation, Long> {
	
	@Query(value = "SELECT cr FROM CustomerRelation cr where cr.isDelete = false order by name")//pakai java class
	List<CustomerRelation> findByName();
	
	//PAGING//
	@Query(value = "select * from m_customer_relation where lower(name) like lower(concat('%',?1,'%')) and is_delete = ?2 order by name asc", nativeQuery = true)
	List<CustomerRelation> findByIsDelete(String keyword, Boolean isDelete);
	
	@Query(value = "select * from m_customer_relation where is_delete = false and name = ?1", nativeQuery=true)
	CustomerRelation findByIdName(String name);

	@Query(value = "select * from m_customer_relation where is_delete = false and name = ?1 and id != ?2", nativeQuery=true)
	CustomerRelation findByIdNameForEdit(String name, Long id);
	
}
