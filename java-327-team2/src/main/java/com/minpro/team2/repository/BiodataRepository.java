package com.minpro.team2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.minpro.team2.model.Biodata;

public interface BiodataRepository extends JpaRepository<Biodata, Long>{

	
	
}
