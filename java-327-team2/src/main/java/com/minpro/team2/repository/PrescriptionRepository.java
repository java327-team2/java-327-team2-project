package com.minpro.team2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.Prescription;

public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

	@Query(value="select * from t_prescription where appointment_id = ?1 order by id asc",nativeQuery=true)
	public List<Prescription> getPrescriptionByAppointmentId(Long appointmentId);
	
}
