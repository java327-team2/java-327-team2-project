package com.minpro.team2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{

	@Query(value = "select * from m_customer where biodata_id = ?1", nativeQuery=true)
	Customer findByBiodataId(Long id);
	
}
