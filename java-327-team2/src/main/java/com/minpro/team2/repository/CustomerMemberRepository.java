package com.minpro.team2.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.minpro.team2.model.CustomerMember;
import com.minpro.team2.model.CustomerRelation;

public interface CustomerMemberRepository extends JpaRepository<CustomerMember, Long> {

	@Query(value = "select * from m_customer_member where parent_biodata_id = ?1 and is_delete = false", nativeQuery = true)
	public List<CustomerMember> findByParentBioId(Long id);

	// PAGING//
	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by b.fullName asc", nativeQuery = true)
	Page<CustomerMember> findByIsDelete(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by b.fullName desc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by c.bod desc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteUsia(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by c.bod asc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteUsiaDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_relation cr on cm.customer_relation_id = cr.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by cr.name asc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteRelasi(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id join m_customer_relation cr on cm.customer_relation_id = cr.id where lower(b.fullName) like lower(concat('%',?1,'%')) and cm.is_delete = ?2 and cm.parent_biodata_id = ?3 order by cr.name desc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteRelasiDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm \r\n"
			+ "join m_customer c on cm.customer_id = c.id \r\n"
			+ "join m_biodata b on c.biodata_id = b.id \r\n"
			+ "left join (select count(cc.id) as count_chat, cc.customer_id from t_customer_chat cc group by cc.customer_id) a on a.customer_id = c.id \r\n"
			+ "where lower(b.fullName) like lower(concat('%','','%'))\r\n"
			+ "and cm.is_delete = false and b.is_delete = false and c.is_delete = false \r\n"
			+ "and cm.parent_biodata_id = 1 group by cm.id,c.id,b.id order by count(a.count_chat) asc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteChat(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm \r\n"
			+ "join m_customer c on cm.customer_id = c.id \r\n"
			+ "join m_biodata b on c.biodata_id = b.id \r\n"
			+ "left join (select count(cc.id) as count_chat, cc.customer_id from t_customer_chat cc group by cc.customer_id) a on a.customer_id = c.id \r\n"
			+ "where lower(b.fullName) like lower(concat('%','','%'))\r\n"
			+ "and cm.is_delete = false and b.is_delete = false and c.is_delete = false \r\n"
			+ "and cm.parent_biodata_id = 1 group by cm.id,c.id,b.id order by count(a.count_chat) desc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteChatDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm \r\n"
			+ "join m_customer c on cm.customer_id = c.id \r\n"
			+ "join m_biodata b on c.biodata_id = b.id \r\n"
			+ "left join (select count(cc.id) as count_appointment, cc.customer_id from t_appointment cc group by cc.customer_id) a on a.customer_id = c.id \r\n"
			+ "where lower(b.fullName) like lower(concat('%','','%'))\r\n"
			+ "and cm.is_delete = false and b.is_delete = false and c.is_delete = false \r\n"
			+ "and cm.parent_biodata_id = 1 group by cm.id,c.id,b.id order by count(a.count_appointment) asc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteAppointment(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm \r\n"
			+ "join m_customer c on cm.customer_id = c.id \r\n"
			+ "join m_biodata b on c.biodata_id = b.id \r\n"
			+ "left join (select count(cc.id) as count_appointment, cc.customer_id from t_appointment cc group by cc.customer_id) a on a.customer_id = c.id \r\n"
			+ "where lower(b.fullName) like lower(concat('%','','%'))\r\n"
			+ "and cm.is_delete = false and b.is_delete = false and c.is_delete = false \r\n"
			+ "and cm.parent_biodata_id = 1 group by cm.id,c.id,b.id order by count(a.count_appointment) desc", nativeQuery = true)
	Page<CustomerMember> findByIsDeleteAppointmentDESC(String keyword, Boolean isDelete, Long id, Pageable page);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) = lower(?1) and cm.is_delete = false limit 1", nativeQuery = true)
	CustomerMember findByIdName(String name);

	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) = lower(?1) and cm.id != ?2 and cm.is_delete = false limit 1", nativeQuery = true)
	CustomerMember findByIdNameForEdit(String name, Long id);
	
	@Query(value = "select cm.* from m_customer_member cm join m_customer c on cm.customer_id = c.id join m_biodata b on c.biodata_id = b.id  where lower(b.fullName) = lower(?1) and cm.id != ?2 and cm.is_delete = false and cm.parent_biodata_id = ?3 limit 1", nativeQuery = true)
	CustomerMember findByIdNameForEditNew(String name, Long id, Long userId);

}
