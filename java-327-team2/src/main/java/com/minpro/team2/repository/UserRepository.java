package com.minpro.team2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.minpro.team2.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	
	
}
