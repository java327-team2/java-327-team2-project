package com.minpro.team2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.minpro.team2.model.BloodGroup;

public interface BloodGroupRepository extends JpaRepository<BloodGroup, Long>{
	
	
	
}
