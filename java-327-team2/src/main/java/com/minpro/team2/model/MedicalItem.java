package com.minpro.team2.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_medical_item")
public class MedicalItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "name")
	public String name;

	@Column(name = "medical_item_category_id")
	public Long medicalItemCategoryId;

	@Column(name = "composition")
	public String composition;

	@Column(name = "medical_item_segmentation_id")
	public Long medicalItemSegmentationId;

	@Column(name = "manufacturer")
	public String manufacturer;

	@Column(name = "indication")
	public String indication;

	@Column(name = "dosage")
	public String dosage;

	@Column(name = "directions")
	public String directions;

	@Column(name = "contraindication")
	public String contraindication;

	@Column(name = "caution")
	public String caution;

	@Column(name = "packaging")
	public String packaging;

	@Column(name = "price_max")
	public Long priceMax;

	@Column(name = "price_min")
	public Long priceMin;

	@Column(name = "image_path")
	public String imagePath;

	@Column(name = "created_by")
	public Long createdBy;

	@Column(name = "created_on")
	public Date createdOn;

	@Column(name = "modified_by")
	public Long modifiedBy;

	@Column(name = "modified_on")
	public Date modifiedOn;

	@Column(name = "deleted_by")
	public Long deletedBy;

	@Column(name = "deleted_on")
	public Date deletedOn;

	@Column(name = "is_delete")
	public Boolean isDelete;

	@ManyToOne
	@JoinColumn(name = "medical_item_category_id", insertable = false, updatable = false)
	public MedicalItemCategory medicalItemCategory;

	@ManyToOne
	@JoinColumn(name = "medical_item_segmentation_id", insertable = false, updatable = false)
	public MedicalItemSegmentation medicalItemSegmentation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMedicalItemCategoryId() {
		return medicalItemCategoryId;
	}

	public void setMedicalItemCategoryId(Long medicalItemCategoryId) {
		this.medicalItemCategoryId = medicalItemCategoryId;
	}

	public String getComposition() {
		return composition;
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

	public Long getMedicalItemSegmentationId() {
		return medicalItemSegmentationId;
	}

	public void setMedicalItemSegmentationId(Long medicalItemSegmentationId) {
		this.medicalItemSegmentationId = medicalItemSegmentationId;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public String getContraindication() {
		return contraindication;
	}

	public void setContraindication(String contraindication) {
		this.contraindication = contraindication;
	}

	public String getCaution() {
		return caution;
	}

	public void setCaution(String caution) {
		this.caution = caution;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public Long getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(Long priceMax) {
		this.priceMax = priceMax;
	}

	public Long getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(Long priceMin) {
		this.priceMin = priceMin;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(Long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public MedicalItemCategory getMedicalItemCategory() {
		return medicalItemCategory;
	}

	public void setMedicalItemCategory(MedicalItemCategory medicalItemCategory) {
		this.medicalItemCategory = medicalItemCategory;
	}

	public MedicalItemSegmentation getMedicalItemSegmentation() {
		return medicalItemSegmentation;
	}

	public void setMedicalItemSegmentation(MedicalItemSegmentation medicalItemSegmentation) {
		this.medicalItemSegmentation = medicalItemSegmentation;
	}

}
