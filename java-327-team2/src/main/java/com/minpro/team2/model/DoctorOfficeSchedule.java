package com.minpro.team2.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_doctor_office_schedule")
public class DoctorOfficeSchedule {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "doctor_id")
	public Long doctorId;

	@Column(name = "medical_facility_schedule_id")
	public Long medicalFacilityScheduleId;

	@Column(name = "slot")
	public Integer slot;

	@Column(name = "created_by")
	public Long createdBy;

	@Column(name = "created_on")
	public Date createdOn;

	@Column(name = "modified_by")
	public Long modifiedBy;

	@Column(name = "modified_on")
	public Date modifiedOn;

	@Column(name = "deleted_by")
	public Long deletedBy;

	@Column(name = "deleted_on")
	public Date deletedOn;

	@Column(name = "is_delete")
	public Boolean isDelete;
	
	

}
