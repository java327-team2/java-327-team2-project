package com.minpro.team2.model;

import java.util.Date;

public class VMCustomerMember {
	
	public Long id;
	
	public Long userId;
	
	public String fullName;
	
	public Date dob;
	
	public String gender;
	
	public Long bloodGroupId;
	
	public String rhesusType;
	
	public double height;
	
	public double weight;
	
	public Long customerRelationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	public String getRhesusType() {
		return rhesusType;
	}

	public void setRhesusType(String rhesusType) {
		this.rhesusType = rhesusType;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Long getCustomerRelationId() {
		return customerRelationId;
	}

	public void setCustomerRelationId(Long customerRelationId) {
		this.customerRelationId = customerRelationId;
	}

	
	
	

}
